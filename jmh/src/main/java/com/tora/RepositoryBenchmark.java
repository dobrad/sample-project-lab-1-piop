package com.tora;

import com.tora.entities.Order;
import com.tora.repositories.*;
import org.openjdk.jmh.annotations.*;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;

import java.util.concurrent.TimeUnit;

@BenchmarkMode(Mode.Throughput)
@OutputTimeUnit(TimeUnit.SECONDS)
@Warmup(iterations = 5, time = 2, timeUnit = TimeUnit.SECONDS)
@Measurement(iterations = 5, time = 2, timeUnit = TimeUnit.SECONDS, batchSize = 100)
@Threads(5)
public class RepositoryBenchmark {
    @State(Scope.Benchmark)
    public static class OrderState {
        Order order = new Order(1, 2, 3);
    }

    @State(Scope.Benchmark)
    public static class TreeSetState {
        InMemoryRepository<Order> repository = new TreeSetBasedRepository<>();
    }

    @State(Scope.Benchmark)
    public static class ArrayListState {
        InMemoryRepository<Order> repository = new ArrayListBasedRepository<>();
    }

    @State(Scope.Benchmark)
    public static class ConcurrentHashMapState {
        InMemoryRepository<Order> repository = new ConcurrentHashMapBasedRepository<>();
    }

    @State(Scope.Benchmark)
    public static class HashSetState {
        InMemoryRepository<Order> repository = new HashSetBasedRepository<>();
    }

    @State(Scope.Benchmark)
    public static class TreeSortedMapState {
        InMemoryRepository<Order> repository = new TreeSortedMapBasedRepository<>();
    }

    @State(Scope.Benchmark)
    public static class O2OArrayMapBasedRepositoryState {
        InMemoryRepository<Order> repository = new O2OArrayMapBasedRepository<>();
    }

    @State(Scope.Benchmark)
    public static class HashObjObjMapBasedRepositoryState {
        InMemoryRepository<Order> repository = new HashObjObjMapBasedRepository<>();
    }


    // add

    @Benchmark
    public void addArrayList(ArrayListState state, OrderState order) {
        state.repository.add(order.order);
    }


    @Benchmark
    public void addConcurrentHashMap(ConcurrentHashMapState state, OrderState order) {
        state.repository.add(order.order);
    }

    @Benchmark
    public void addHashSet(HashSetState state, OrderState order) {
        state.repository.add(order.order);
    }


    @Benchmark
    public void addTreeSet(TreeSetState state, OrderState order) {
        state.repository.add(order.order);
    }

    @Benchmark
    public void addTreeSortedMap(TreeSortedMapState state, OrderState order) {
        state.repository.add(order.order);
    }

    @Benchmark
    public void addO2OArrayMapBasedRepository(O2OArrayMapBasedRepositoryState state, OrderState order) {
        state.repository.add(order.order);
    }

    @Benchmark
    public void addHashObjObjMapBasedRepository(HashObjObjMapBasedRepositoryState state, OrderState order) {
        state.repository.add(order.order);
    }


    // contains

    @Benchmark
    public void containsArrayList(ArrayListState state, OrderState order) {
        state.repository.add(order.order);
        state.repository.contains(order.order);
    }

    @Benchmark
    public void containsConcurrentHashMap(ConcurrentHashMapState state, OrderState order) {
        state.repository.add(order.order);
        state.repository.contains(order.order);
    }

    @Benchmark
    public void containsHashSet(HashSetState state, OrderState order) {
        state.repository.add(order.order);
        state.repository.contains(order.order);
    }

    @Benchmark
    public void containsTreeSet(TreeSetState state, OrderState order) {
        state.repository.add(order.order);
        state.repository.contains(order.order);
    }

    @Benchmark
    public void containsTreeSortedMap(TreeSortedMapState state, OrderState order) {
        state.repository.add(order.order);
        state.repository.contains(order.order);
    }

    @Benchmark
    public void containsO2OArrayMapBasedRepository(O2OArrayMapBasedRepositoryState state, OrderState order) {
        state.repository.add(order.order);
        state.repository.contains(order.order);
    }

    @Benchmark
    public void containsHashObjObjMapBasedRepository(HashObjObjMapBasedRepositoryState state, OrderState order) {
        state.repository.add(order.order);
        state.repository.contains(order.order);
    }


    // remove

    @Benchmark
    public void removeArrayList(ArrayListState state, OrderState order) {
        state.repository.add(order.order);
        state.repository.remove(order.order);
    }


    @Benchmark
    public void removeConcurrentHashMap(ConcurrentHashMapState state, OrderState order) {
        state.repository.remove(order.order);
    }

    @Benchmark
    public void removeHashSet(HashSetState state, OrderState order) {
        state.repository.remove(order.order);
    }


    @Benchmark
    public void removeTreeSet(TreeSetState state, OrderState order) {
        state.repository.remove(order.order);
    }

    @Benchmark
    public void removeTreeSortedMap(TreeSortedMapState state, OrderState order) {
        state.repository.remove(order.order);
    }

    @Benchmark
    public void removeO2OArrayMapBasedRepository(O2OArrayMapBasedRepositoryState state, OrderState order) {
        state.repository.add(order.order);
        state.repository.remove(order.order);
    }

    @Benchmark
    public void removeHashObjObjMapBasedRepository(HashObjObjMapBasedRepositoryState state, OrderState order) {
        state.repository.add(order.order);
        state.repository.remove(order.order);
    }


    public static void main(String[] args) throws RunnerException {
        Options options = new OptionsBuilder().include(RepositoryBenchmark.class.getSimpleName()).forks(1).build();

        new Runner(options).run();
    }
}
