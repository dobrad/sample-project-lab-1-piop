package com.tora.repositories;

import java.util.TreeSet;

public class TreeSetBasedRepository<T> implements InMemoryRepository<T> {

    private final TreeSet<T> set;

    public TreeSetBasedRepository() {
        set = new TreeSet<>();
    }

    @Override
    public void add(T object) {
        set.add(object);
    }

    @Override
    public boolean contains(T object) {
        return set.contains(object);
    }

    @Override
    public void remove(T object) {
        set.remove(object);
    }
}
