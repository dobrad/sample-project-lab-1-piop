package com.tora.repositories;

import java.util.HashSet;

public class HashSetBasedRepository<T> implements InMemoryRepository<T> {

    private final HashSet<T> set;

    public HashSetBasedRepository() {
        set = new HashSet<>();
    }

    @Override
    public void add(T object) {
        set.add(object);
    }

    @Override
    public boolean contains(T object) {
        return set.contains(object);
    }

    @Override
    public void remove(T object) {
        set.remove(object);
    }
}
