package com.tora.repositories;

import com.tora.entities.BaseEntity;

import java.util.concurrent.ConcurrentHashMap;

public class ConcurrentHashMapBasedRepository<T extends BaseEntity> implements InMemoryRepository<T> {

    private final ConcurrentHashMap<Integer, T> hashMap;

    public ConcurrentHashMapBasedRepository() {
        hashMap = new ConcurrentHashMap<>();
    }

    @Override
    public void add(T object) {
        hashMap.put(object.getId(), object);
    }

    @Override
    public boolean contains(T object) {
        return hashMap.containsKey(object.getId());
    }

    @Override
    public void remove(T object) {
        hashMap.remove(object.getId());
    }
}
