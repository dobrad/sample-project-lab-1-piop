package com.tora.repositories;

import com.tora.entities.BaseEntity;
import it.unimi.dsi.fastutil.objects.Object2ObjectArrayMap;

public class O2OArrayMapBasedRepository<T extends BaseEntity> implements InMemoryRepository<T> {

    private final Object2ObjectArrayMap<Integer, T> arrayMap;

    public O2OArrayMapBasedRepository() {
        this.arrayMap = new Object2ObjectArrayMap<>();
    }

    @Override
    public void add(T object) {
        synchronized (this) {
            this.arrayMap.put(object.getId(), object);
        }
    }

    @Override
    public boolean contains(T object) {
        synchronized (this) {
            return this.arrayMap.containsKey(object.getId());
        }
    }

    @Override
    public void remove(T object) {
        synchronized (this) {
            this.arrayMap.remove(object.getId());
        }
    }
}
