package com.tora.repositories;

import java.util.ArrayList;

public class ArrayListBasedRepository<T> implements InMemoryRepository<T> {

    private final ArrayList<T> list;

    public ArrayListBasedRepository() {
        this.list = new ArrayList<>();
    }

    @Override
    public void add(T object) {
        synchronized (this) {
            list.add(object);
        }
    }

    @Override
    public boolean contains(T object) {
        synchronized (this) {
            return list.contains(object);
        }
    }

    @Override
    public void remove(T object) {
        synchronized (this) {
            list.remove(object);
        }
    }
}
