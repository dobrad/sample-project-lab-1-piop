package com.tora.repositories;

import com.koloboke.collect.map.hash.HashObjObjMap;
import com.koloboke.collect.map.hash.HashObjObjMaps;
import com.tora.entities.BaseEntity;

public class HashObjObjMapBasedRepository<T extends BaseEntity> implements InMemoryRepository<T> {

    private final HashObjObjMap<Integer, T> map;

    public HashObjObjMapBasedRepository() {
        this.map = HashObjObjMaps.<Integer,T>newMutableMap();
    }

    @Override
    public void add(T object) {
        map.put(object.getId(),object);
    }

    @Override
    public boolean contains(T object) {
        return map.containsKey(object.getId());
    }

    @Override
    public void remove(T object) {
        map.remove(object.getId());
    }
}
