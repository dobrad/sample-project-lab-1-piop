package com.tora.repositories;

import com.tora.entities.BaseEntity;
import org.eclipse.collections.impl.map.sorted.mutable.TreeSortedMap;

public class TreeSortedMapBasedRepository<T extends BaseEntity> implements InMemoryRepository<T> {

    private final TreeSortedMap<Integer, T> sortedMap;

    public TreeSortedMapBasedRepository() {
        sortedMap = new TreeSortedMap<>();
    }

    @Override
    public void add(T object) {
        sortedMap.put(object.getId(), object);
    }

    @Override
    public boolean contains(T object) {
        return sortedMap.containsKey(object.getId());
    }

    @Override
    public void remove(T object) {
        sortedMap.remove(object.getId());
    }
}
