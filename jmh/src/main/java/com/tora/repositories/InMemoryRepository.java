package com.tora.repositories;

public interface InMemoryRepository <T> {
    void add(T object);
    boolean contains(T object);
    void remove(T object);
}
