package com.tora;

import com.tora.entitites.Calculator;
import com.tora.services.CalculatorService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class CalculatorServiceTest {

    private CalculatorService service;

    @Before
    public void setup() {
        var calculator = new Calculator(10);
        service = new CalculatorService(calculator);
    }

    @Test
    public void testDivisionBy0() {
        Assert.assertThrows(RuntimeException.class, () -> service.performOperation('/', 0));
    }

    @Test
    public void testGetCalculatorResult() {
        Assert.assertEquals(10, service.getCalculatorResult(), 0.0);
    }

    @Test
    public void testResetCalculatorResult() {
        service.resetCalculatorResult();
        Assert.assertEquals(0, service.getCalculatorResult(), 0.0);
    }

    @Test
    public void testAddOperation() {
        service.performOperation('+', 10);
        Assert.assertEquals(20, service.getCalculatorResult(), 0.0);
    }

    @Test
    public void testSubtractOperation() {
        service.performOperation('-', 10);
        Assert.assertEquals(0, service.getCalculatorResult(), 0.0);
    }

    @Test
    public void testMultiplyOperation() {
        service.performOperation('*', 10);
        Assert.assertEquals(100, service.getCalculatorResult(), 0.0);
    }

    @Test
    public void testDivideOperation() {
        service.performOperation('/', 10);
        Assert.assertEquals(1, service.getCalculatorResult(), 0.0);
    }
}
