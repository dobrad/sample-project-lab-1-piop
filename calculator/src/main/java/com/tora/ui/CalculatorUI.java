package com.tora.ui;

import com.tora.services.CalculatorService;

import java.util.List;
import java.util.Scanner;

public class CalculatorUI {
    private final static List<Character> VALID_OPERATORS = List.of('+', '-', '*', '/');
    private final CalculatorService service;

    public CalculatorUI(CalculatorService service) {
        this.service = service;
    }

    public void start() {
        printInstructions();

        boolean canContinue = true;
        while (canContinue) {
            printResult();
            try {
                canContinue = scanAndPerformOperation();
            } catch (RuntimeException e) {
                System.out.println("Invalid input!");
            }
        }
    }

    public void printInstructions() {
        System.out.println("Operations: ");
        System.out.println("\texit");
        System.out.println("\treset");
        System.out.printf("\t %s {operand}%n", VALID_OPERATORS);
    }

    public void printResult() {
        System.out.printf("Result: %s%n", service.getCalculatorResult());
        System.out.print(":");
    }

    public boolean scanAndPerformOperation() {
        Scanner scanner = new Scanner(System.in);
        var operation = scanner.nextLine();
        if (operation.equals("exit")) {
            return false;
        } else if (operation.equals("reset")) {
            service.resetCalculatorResult();
        } else {
            operation = operation.replaceAll("\\s", "");
            operation = operation.trim();
            var operator = operation.charAt(0);
            if (!VALID_OPERATORS.contains(operator))
                throw new RuntimeException("Invalid input!");

            double operand;
            try {
                operand = Double.parseDouble(operation.substring(1));
                service.performOperation(operator, operand);
            } catch (NumberFormatException | NullPointerException e) {
                throw new RuntimeException("Invalid input!");
            }
        }
        return true;
    }

}