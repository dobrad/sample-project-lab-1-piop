package com.tora.entitites;

public class Calculator {
    private double result;

    public Calculator() {
        this.result = 0;
    }

    public Calculator(double result) {
        this.result = result;
    }

    public double getResult() {
        return result;
    }

    public void resetResult() {
        result = 0;
    }

    public void add(double operand) {
        result += operand;
    }

    public void divide(double operand) {
        if (operand == 0)
            throw new RuntimeException("Can't perform division by 0!");
        result /= operand;
    }

    public void multiply(double operand) {
        result *= operand;
    }

    public void subtract(double operand) {
        result -= operand;
    }
}
