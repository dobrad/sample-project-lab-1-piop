package com.tora;

import com.tora.entitites.Calculator;
import com.tora.services.CalculatorService;
import com.tora.ui.CalculatorUI;

/**
 * Hello world!
 */
public class App {
    public static void main(String[] args) {
        System.out.println("Welcome to Dobra's calculator!");
        var calculator = new Calculator();
        var service = new CalculatorService(calculator);
        var calculatorUI = new CalculatorUI(service);
        calculatorUI.start();
    }
}
