package com.tora.services;

import com.tora.entitites.Calculator;

public class CalculatorService {

    private final Calculator calculator;

    public CalculatorService(Calculator calculator) {
        this.calculator = calculator;
    }

    public void performOperation(char operator, double operand) {
        if (operator == '+') {
            calculator.add(operand);
        } else if (operator == '-') {
            calculator.subtract(operand);
        } else if (operator == '*') {
            calculator.multiply(operand);
        } else if (operator == '/') {
            calculator.divide(operand);
        }
    }

    public double getCalculatorResult() {
        return calculator.getResult();
    }

    public void resetCalculatorResult() {
        calculator.resetResult();
    }
}
