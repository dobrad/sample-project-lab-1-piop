package com.tora.server;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;

public class ServerThreadConnection {
    private final Socket socket;
    private final PrintWriter printWriter;

    public ServerThreadConnection(Socket socket) throws IOException {
        this.socket = socket;
        printWriter = new PrintWriter(this.socket.getOutputStream(), true);
    }

    public PrintWriter getPrintWriter() {
        return printWriter;
    }
}
