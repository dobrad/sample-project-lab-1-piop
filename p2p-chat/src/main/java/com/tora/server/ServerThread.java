package com.tora.server;

import java.io.IOException;
import java.net.ServerSocket;
import java.util.HashSet;
import java.util.Set;

public class ServerThread extends Thread {
    private final ServerSocket serverSocket;
    private final Set<ServerThreadConnection> serverThreadConnections = new HashSet<>();

    public ServerThread(String port) throws IOException {
        serverSocket = new ServerSocket(Integer.parseInt(port));
    }

    public void sendMessage(String message) { // send the message to all connections
        serverThreadConnections.forEach(t -> t.getPrintWriter().println(message));
    }

    @Override
    public void run() {
        try {
            while (true) {
                ServerThreadConnection serverThreadConnection = new ServerThreadConnection(serverSocket.accept());
                serverThreadConnections.add(serverThreadConnection);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
