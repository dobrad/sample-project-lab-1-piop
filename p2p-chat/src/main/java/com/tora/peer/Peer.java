package com.tora.peer;

import com.tora.server.ServerThread;

import javax.json.Json;
import javax.json.JsonException;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.net.Socket;
import java.util.Objects;

public class Peer {
    public static void main(String[] args) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Enter a username and a port for the current peer, separated by a space:");
        String[] config = bufferedReader.readLine().split(" ");

        ServerThread serverThread = new ServerThread(config[1]);
        serverThread.start();

        new Peer().changePeers(bufferedReader, config[0], serverThread);
    }

    private void changePeers(BufferedReader bufferedReader, String username, ServerThread serverThread) throws IOException {
        System.out.println("Enter a space separated list of HOSTNAME:PORT values to receive messages from");
        System.out.println(" - skip - To skip this step");

        String input = bufferedReader.readLine();
        String[] inputTokens = input.split(" ");

        if (!input.equalsIgnoreCase("skip")) {
            for (String inputToken : inputTokens) {
                String[] address = inputToken.split(":");
                Socket socket = null;
                try {
                    socket = new Socket(address[0], Integer.parseInt(address[1]));
                    new PeerThread(socket).start();
                } catch (IOException | NumberFormatException e) {
                    e.printStackTrace();
                    if (Objects.nonNull(socket))
                        socket.close();
                    else
                        System.out.println("Invalid input. Skipping to next peer address");
                }
            }
        }
        communicate(bufferedReader, username, serverThread);

    }

    public void communicate(BufferedReader bufferedReader, String username, ServerThread serverThread) {
        try {
            System.out.println("You can now send and receive messages!\n Commands:\n\t- exit\n\t- change -> To change peers from which to get messages\n");
            boolean flag = true;
            while (flag) {
//                System.out.print(">");
                String message = bufferedReader.readLine();
                if (message.equalsIgnoreCase("exit")) {
                    flag = false;
                } else if (message.equalsIgnoreCase("change")) {
                    changePeers(bufferedReader, username, serverThread);
                } else {
                    StringWriter stringWriter = new StringWriter();
                    Json.createWriter(stringWriter).writeObject(
                            Json.createObjectBuilder()
                                    .add("username", username)
                                    .add("message", message)
                                    .build());
                    serverThread.sendMessage(stringWriter.toString());
                }
            }
        } catch (IOException | JsonException e) {
            e.printStackTrace();
            System.exit(-1);
        }
    }
}
