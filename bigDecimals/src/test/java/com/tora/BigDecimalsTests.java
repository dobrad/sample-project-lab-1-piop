package com.tora;


import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static com.tora.BigDecimalSerialization.deserializeBigDecimal;
import static com.tora.BigDecimalSerialization.serializeBigDecimal;
import static com.tora.BigDecimalUtils.*;

public class BigDecimalsTests {

    public List<BigDecimal> numbers;

    @Before
    public void setup() {
        numbers = new ArrayList<>(
                List.of(
                        new BigDecimal("1.5"),
                        new BigDecimal("2.5"),
                        new BigDecimal("3.5"),
                        new BigDecimal("4.5"),
                        new BigDecimal("5.5"),
                        new BigDecimal("6.5"),
                        new BigDecimal("7.5"),
                        new BigDecimal("8.5"),
                        new BigDecimal("9.5"),
                        new BigDecimal("10.5"),
                        new BigDecimal("11.5"),
                        new BigDecimal("12.5")
                )
        );
    }

    @Test
    public void testComputeSum() {
        Assert.assertEquals(computeSum(numbers), new BigDecimal("84.0"));
    }

    @Test
    public void testComputeAverage() {
        Assert.assertEquals(computeAverage(numbers), new BigDecimal("7.0"));
    }

    @Test
    public void testTop10BigDecimals() {
        Assert.assertEquals(top10BigDecimals(numbers), List.of(numbers.get(numbers.size() - 1)));
    }

    @Test
    public void testSerialization() throws IOException {
        var number = new BigDecimal("11111111111111111.111111111111111111");
        var baseOutputStream = new ByteArrayOutputStream();
        var objectOutputStream = new ObjectOutputStream(baseOutputStream);
        serializeBigDecimal(objectOutputStream, number);
        objectOutputStream.close();

        var baseInputStream = new ByteArrayInputStream(baseOutputStream.toByteArray());
        var objectInputStream = new ObjectInputStream(baseInputStream);
        var deserialized = deserializeBigDecimal(objectInputStream);
        objectInputStream.close();

        Assert.assertEquals(number, deserialized);

    }
}
