package com.tora;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;


public class BigDecimalUtils {
    public static BigDecimal computeSum(List<BigDecimal> bigDecimals) {
        return bigDecimals.stream().reduce(BigDecimal.ZERO, BigDecimal::add);
    }

    public static BigDecimal computeAverage(List<BigDecimal> bigDecimals) {
        return computeSum(bigDecimals).divide(BigDecimal.valueOf(bigDecimals.size()), RoundingMode.CEILING);
    }

    public static List<BigDecimal> top10BigDecimals(List<BigDecimal> bigDecimals) {
        return bigDecimals.stream()
                .sorted(Comparator.reverseOrder())
                .limit(10L * bigDecimals.size() / 100)
                .collect(Collectors.toList());
    }
}
