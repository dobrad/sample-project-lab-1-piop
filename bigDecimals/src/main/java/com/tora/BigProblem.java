package com.tora;

import java.io.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.IntStream;

import static com.tora.BigDecimalSerialization.deserializeBigDecimal;
import static com.tora.BigDecimalSerialization.serializeBigDecimal;
import static com.tora.BigDecimalUtils.*;

public class BigProblem {
    public static List<BigDecimal> numbers;
    public static int nrCount;

    public static void main(String[] args) {
        generateAndSerializeNumbers();
        deserializeNumbers();
        operateBigDecimalList();
    }

    private static void deserializeNumbers() {
        numbers = new ArrayList<>();
        try (
                var baseInputStream = new FileInputStream("serialized.txt");
                var objectInputStream = new ObjectInputStream(baseInputStream)
        ) {
            IntStream.range(0, nrCount)
                    .mapToObj(i -> {
                        try {
                            return deserializeBigDecimal(objectInputStream);
                        } catch (IOException e) {
                            throw new RuntimeException(e);
                        }
                    })
                    .forEach(number -> numbers.add(number));

        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private static void generateAndSerializeNumbers() {
        var random = new Random();

        nrCount = random.nextInt(9000) + 1000;
        int intPartDigitNr = random.nextInt(30) + 10;
        int decPartDigitNr = random.nextInt(20) + 10;
        try (
                var serializedFileOutputStream = new FileOutputStream("serialized.txt");
                var serializedObjectOutputStream = new ObjectOutputStream(serializedFileOutputStream);
        ) {
            for (int i = 0; i < nrCount; i++) {
                var numberString = new StringBuilder();

                for (int j = 0; j < intPartDigitNr; j++) {
                    if (j == 0)
                        numberString.append(random.nextInt(9) + 1);
                    numberString.append(random.nextInt(10));
                }

                numberString.append(".");

                for (int j = 0; j < decPartDigitNr; j++) {
                    numberString.append(random.nextInt(10));
                }

                var number = new BigDecimal(numberString.toString());
                serializeBigDecimal(serializedObjectOutputStream, number);
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private static void operateBigDecimalList() {
        System.out.println(nrCount);
        System.out.println(computeSum(numbers));
        System.out.println(computeAverage(numbers));
        System.out.println(top10BigDecimals(numbers));
    }


}