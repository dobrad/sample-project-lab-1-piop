package com.tora;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.math.BigDecimal;
import java.math.BigInteger;

public class BigDecimalSerialization {
    public static void serializeBigDecimal(ObjectOutputStream outputStream, BigDecimal bigDecimal) throws IOException {
        var unscaledBytes = bigDecimal.unscaledValue().toByteArray();

        outputStream.writeInt(unscaledBytes.length);
        outputStream.write(unscaledBytes);
        outputStream.writeInt(bigDecimal.scale());
    }

    public static BigDecimal deserializeBigDecimal(ObjectInputStream inputStream) throws IOException {
        var unscaledBytesLength = inputStream.readInt();
        var unscaledBytes = inputStream.readNBytes(unscaledBytesLength);
        var scale = inputStream.readInt();

        var unscaledValue = new BigInteger(unscaledBytes);
        return new BigDecimal(unscaledValue, scale);
    }
}
